package WeatherState;

public class WeatherState {
    private String time; // время
    private String wthConditions; // погодные явления
    private String temperature; // температура
    private String windSpeed; // скорость ветра
    private String windDirection; // направление ветра
    private String precipitation; // осадки в мм
    private String pressure; // атм. давление
    private String humidity; // влажность воздуха

    public WeatherState() {
    }

    public WeatherState(String time, String wthConditions, String temp, String windSpeed, String windDirection,
                        String precip, String pressure, String humidity) {
        this.time = time;
        this.wthConditions = wthConditions;
        this.temperature = temp;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
        this.precipitation = precip;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setWthConditions(String wthConditions) {
        this.wthConditions = wthConditions;
    }

    public void setTemperature(String temp) {
        this.temperature = temp;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public void setPrecipitation(String precip) {
        this.precipitation = precip;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getTime() {
        return this.time;
    }

    public String getWthConditions() {
        return this.wthConditions;
    }

    public String getTemperature() {
        return this.temperature;
    }

    public String getWindSpeed() {
        return this.windSpeed;
    }

    public String getWindDirection() {
        return this.windDirection;
    }

    public String getPrecipitation() {
        return this.precipitation;
    }

    public String getPressure() {
        return this.pressure;
    }

    public String getHumidity() {
        return this.humidity;
    }

    public void print() {
        System.out.println(time + "\t" + wthConditions + "\t" + temperature + "\t" + windSpeed + "\t" +
                windDirection + "\t" + precipitation + "\t" + pressure + "\t" + humidity);
    }
}
