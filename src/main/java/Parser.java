import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Attributes;
import org.jsoup.select.Elements;

import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;

import WeatherState.WeatherState;
import WeatherForecast.GismeteoWeatherForecast;

public class Parser {

    private static Document getPage() throws IOException {
        //String url = "https://pogoda.by/weather/numerical-weather-2";
        //String url = "https://pogoda.spb.ru";
        String url = "https://www.gismeteo.by/weather-minsk-4248/";
        //String url = "https://www.gismeteo.by/weather-minsk-4248/4-day/";
        Document page = Jsoup.parse(new URL(url), 4000);
        return page;
    }

    public static void main(String[] args) throws IOException {
        Document page = getPage();
        Element page_title = page.selectFirst("div[class=page-title]");
        String title = page_title.text();

        GismeteoWeatherForecast gismeteo = new GismeteoWeatherForecast(title);
        WeatherState[] states = new WeatherState[8];

        Element wthTable = page.select("div[class=widget-wrap widget-menu-wrap]").first();

        Element timesRow = wthTable.selectFirst("div[class=widget-row widget-row-time]");
        Element wthConditionsRow = wthTable.selectFirst("div[class=widget-row widget-row-icon]");
        Element temperatureRow = wthTable.selectFirst("div[class=widget-row-chart widget-row-chart-temperature]");
        Element windSpeedRow = wthTable.selectFirst("div[class=widget-row widget-row-wind-speed-gust row-with-caption]");
        Element precipitationRow = wthTable.selectFirst("div[class=widget-row widget-row-precipitation-bars row-with-caption]");

        int i = 0;
        Elements times = timesRow.select("div[class=row-item]");
        String[] times_ = new String[8];
        for (Element time : times) {
            String t = time.text();
            t = t.substring(0, t.length() - 2);
            times_[i++] = t;
        }

        Elements wthConditions = wthConditionsRow.select("div[class=weather-icon tooltip]");
        String[] wthConditions_ = new String[8];
        i = 0;
        for (Element el : wthConditions) {
            Attributes ats = el.attributes();
            wthConditions_[i++] = ats.get("data-text");
        }

        Elements temperature = temperatureRow.select("span[class=unit unit_temperature_c]");//("div[class=values]");
        String[] temperature_ = new String[8];
        i = 0;
        for (Element el : temperature) {
            temperature_[i++] = el.text();
        }

        //Elements windSpeed = windSpeedRow.select("span[class=wind-unit unit unit_wind_m_s]");
        Elements windSpeed = windSpeedRow.select("div[class=row-item]");
        String[] windSpeed_ = new String[8];
        i = 0;
        for (Element el : windSpeed) {
            Element temp = el.selectFirst("span[class=wind-unit unit unit_wind_m_s]");
            if (temp != null) windSpeed_[i++] = temp.text();
            else {
                temp = el.selectFirst("span[class=wind-unit unit unit_wind_m_s wind-zero]");
                windSpeed_[i++] = temp.text();
            }
        }

        Elements precipitation = precipitationRow.select("div[class=item-unit]");
        String[] precipitation_ = new String[8];
        i = 0;
        for (Element el : precipitation) {
            precipitation_[i++] = el.text();
        }

        wthTable = page.select("div[class=widget widget-wind widget-oneday]").first();
        Element windDirecRow = wthTable.selectFirst("div[class=widget-row widget-row-wind-direction]");
        Elements directions = windDirecRow.select("div[class=row-item]");
        String[] directions_ = new String[8];
        i = 0;
        for (Element el : directions) {
            Element temp = el.selectFirst("div[class=direction]");
            if(temp!=null) directions_[i++] = temp.text();
            else directions_[i++] = "н/д";
        }

        wthTable = page.select("div[class=widget widget-pressure widget-oneday]").first();
        Element pressureRow = wthTable.selectFirst("div[class=widget-row-chart widget-row-chart-pressure]");
        Elements pressure = pressureRow.select("div[class=value]");
        String[] pressure_ = new String[8];
        i = 0;
        for (Element el : pressure) {
            Element temp = el.selectFirst("span[class=unit unit_pressure_mm_hg_atm]");
            if(temp!=null) pressure_[i++] = temp.text();
            else pressure_[i++] = "no data";
        }

        wthTable = page.select("div[class=widget widget-humidity widget-oneday]").first();
        Element humidityRow = wthTable.selectFirst("div[class=widget-row widget-row-humidity]");
        String str = humidityRow.text();
        String[] humidity_ = new String[8];
        humidity_ = str.split(" ");


        for (i = 0; i < 8; i++) {
            states[i] = new WeatherState(times_[i], wthConditions_[i], temperature_[i], windSpeed_[i],
                    directions_[i], precipitation_[i], pressure_[i], humidity_[i]);
        }

        gismeteo.setWthStates(states);
        gismeteo.print_info();

        //System.out.println(wthTable);
        //System.out.println(page);
    }
}
