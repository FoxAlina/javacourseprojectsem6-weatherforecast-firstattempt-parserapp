package WeatherForecast;

import WeatherState.WeatherState;

public class GismeteoWeatherForecast {
    private String title;
    private WeatherState[] wthStates;

    public GismeteoWeatherForecast() {
    }

    public GismeteoWeatherForecast(String title) {
        this.title = title;
    }

    public GismeteoWeatherForecast(String title, WeatherState[] states) {
        this.title = title;
        this.wthStates = states;
    }

    public void setWthStates(WeatherState[] wthStates){
        this.wthStates = wthStates;
    }

    public void print_info() {
        System.out.println(title);
        for (WeatherState temp : wthStates) {
            temp.print();
        }
    }
}
