package WeatherForecast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public interface Parsable {
    Document getPage() throws IOException;
    void parsePage();
}
