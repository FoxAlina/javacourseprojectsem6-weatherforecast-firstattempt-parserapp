package WeatherForecast;

import WeatherState.WeatherState;
import WeatherForecast.Parsable;

public abstract class WeatherForecast{
    private String url;
    private String title;
    private WeatherState[] wthStates;

    public WeatherForecast() {
    }

    public WeatherForecast(String url) {
        this.url = url;
    }

    public WeatherForecast(String title, WeatherState[] states) {
        this.title = title;
        this.wthStates = states;
    }

    public void setWthStates(WeatherState[] wthStates){
        this.wthStates = wthStates;
    }

    public void print_info() {
        System.out.println(title);
        for (WeatherState temp : wthStates) {
            temp.print();
        }
    }
}
